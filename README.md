# F#: AVL Set

## Реализация
```fsharp
module AVL

type AVLTree<'T when 'T: comparison> =
    | Empty
    | Node of 'T * AVLTree<'T> * AVLTree<'T> * int

...

let rec insert value =
    function
    | Empty -> Node(value, Empty, Empty, 1)
    | Node(v, l, r, _) as node ->
        if value < v then
            balance <| createNode v (insert value l) r
        elif value > v then
            balance <| createNode v l (insert value r)
        else
            node

let rec remove value =
    function
    | Empty -> Empty
    | Node(v, l, r, _) as node ->
        if value < v then
            balance <| createNode v (remove value l) r
        elif value > v then
            balance <| createNode v l (remove value r)
        else
            match l, r with
            | Empty, _ -> r
            | _, Empty -> l
            | _ ->
                let minRight = findMin r
                balance <| createNode minRight l (removeMin r)

let rec map func =
    function
    | Empty -> Empty
    | Node(v, l, r, _) -> balance <| createNode (func v) (map func l) (map func r)

let rec fold folder state =
    function
    | Empty -> state
    | Node(v, l, r, _) -> fold folder (fold folder (folder state v) l) r

let foldBack folder state =
    function
    | Empty -> state
    | Node(v, l, r, _) -> fold folder (fold folder (folder state v) r) l

let rec mergeWithInsert tree1 tree2 =
    match tree1 with
    | Empty -> tree2
    | Node(v, l, r, _) ->
        let tree2 = mergeWithInsert l tree2
        let tree2 = mergeWithInsert r tree2
        insert v tree2

let rec find value =
    function
    | Empty -> false
    | Node(v, l, r, _) ->
        if value < v then find value l
        elif value > v then find value r
        else true

let rec filter predicate tree =
    match tree with
    | Empty -> Empty
    | Node(value, left, right, _) ->
        let leftFiltered = filter predicate left
        let rightFiltered = filter predicate right

        if predicate value then
            // Если значение удовлетворяет предикату, вставляем его в новое дерево и балансируем.
            mergeWithInsert leftFiltered rightFiltered |> insert value
        else
            // Если значение не удовлетворяет предикату, объединяем фильтрованные левое и правое поддеревья.
            mergeWithInsert leftFiltered rightFiltered

```
Подробнее с исходным кодом AVLTree можно ознакомиться [тут](./fp-lab2/AvlTree.fs)

## Тестирование
### Unit-тесты

Пример unit-теста:
```fsharp
    [<Fact>]
    let ``Проверка вставки элементов с балансировкой AVL дерева`` () =
        // Arrange
        let valuesToInsert = [ 3; 2; 1; 4; 5 ] // порядок вставки создаст несбалансированное дерево - потребует балансировки
        // Act
        let actualTree = List.foldBack insert valuesToInsert Empty
        // Assert
        let balanced = (balanceFactor actualTree) |> abs <= 1 // дерево сбалансирвоано
        balanced |> should be True
```
Подробнее с модульными тестами AVLTree можно ознакомиться [тут](./fp-lab2-test/UnitTests.fs)

### Property-based тесты
Реализованы при помощи библиотеки FsCheck.

Для корректного тестирования определен генератор для структуры AVLTree:
```fsharp
let AvlTreeGenerator =
    gen {
        let values =
            Arb.generate<int>
            |> Gen.sample maxValue count
            |> Seq.toList
            |> List.distinct
            |> listToAVLTree

        return values
    }

type AVLTreeArbitrary() =
    static member Arbitrary: Arbitrary<AVLTree<int>> = Arb.fromGen AvlTreeGenerator
```

И затем проброшен в свойство тестового класса:
```fsharp
[<Properties(Arbitrary = [| typeof<AVLTreeArbitrary> |])>]
type AVLTreeProperties() =
...
```

Примеры property-тестов:
```fsharp
    [<Property>]
    let ``Свойство моноида 1: нейтральный элемент`` (tree: AVLTree<int>) =
        let treeAfterInsertEmpty = mergeWithInsert tree Empty

        equalAVL treeAfterInsertEmpty tree

    [<Property>]
    let ``Свойство моноида 2: ассоциативность бинарной операции``
        (
            tree1Values: list<int>,
            tree2Values: list<int>,
            tree3Values: list<int>
        ) =
        let tree1 = listToAVLTree tree1Values
        let tree2 = listToAVLTree tree2Values
        let tree3 = listToAVLTree tree3Values

        let mergedTree1 = mergeWithInsert (mergeWithInsert tree1 tree2) tree3
        let mergedTree2 = mergeWithInsert tree1 (mergeWithInsert tree2 tree3)

        equalAVL mergedTree1 mergedTree2
```
Подробнее с property-based тестами AVLTree можно ознакомиться [тут](./fp-lab2-test/PropertyBasedTests.fs)

### Запуск тестов

```shell
$ dotnet test -c Release /p:CollectCoverage=true
...
Starting test execution, please wait...

Passed!  - Failed:     0, Passed:    35, Skipped:     0, Total:    35, Duration: 1 m 12 s - fp-lab2-test.dll (net7.0)
+---------+--------+--------+--------+
| Module  | Line   | Branch | Method |
+---------+--------+--------+--------+
| fp-lab2 | 87.01% | 85.71% | 95%    |
+---------+--------+--------+--------+

+---------+--------+--------+--------+
|         | Line   | Branch | Method |
+---------+--------+--------+--------+
| Total   | 87.01% | 85.71% | 95%    |
+---------+--------+--------+--------+
| Average | 87.01% | 85.71% | 95%    |
+---------+--------+--------+--------+
```