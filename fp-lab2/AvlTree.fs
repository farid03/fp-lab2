module AVL

type AVLTree<'T when 'T: comparison> =
    | Empty
    | Node of 'T * AVLTree<'T> * AVLTree<'T> * int

let height =
    function
    | Empty -> 0
    | Node(_, _, _, h) -> h

let createNode value left right =
    Node(value, left, right, max (height left) (height right) + 1)

let balanceFactor =
    function
    | Empty -> 0
    | Node(_, l, r, _) -> height l - height r

let rec balance node =
    match node with
    | Node(v, l, r, _) ->
        let bf = balanceFactor node

        if bf > 1 then
            let lf = balanceFactor l

            if lf >= 0 then
                rotateRight node
            else
                rotateRight <| createNode v (rotateLeft l) r
        elif bf < -1 then
            let rf = balanceFactor r

            if rf <= 0 then
                rotateLeft node
            else
                rotateLeft <| createNode v l (rotateRight r)
        else
            node
    | Empty -> Empty

and rotateRight =
    function
    | Node(v1, Node(v2, l2, r2, _), r1, _) -> createNode v2 l2 (createNode v1 r2 r1)
    | _ -> failwith "rotateRight on Empty or malformed tree"

and rotateLeft =
    function
    | Node(v1, l1, Node(v2, l2, r2, _), _) -> createNode v2 (createNode v1 l1 l2) r2
    | _ -> failwith "rotateLeft on Empty or malformed tree"

let rec insert value =
    function
    | Empty -> Node(value, Empty, Empty, 1)
    | Node(v, l, r, _) as node ->
        if value < v then
            balance <| createNode v (insert value l) r
        elif value > v then
            balance <| createNode v l (insert value r)
        else
            node

let rec findMin =
    function
    | Node(v, Empty, _, _) -> v
    | Node(_, l, _, _) -> findMin l
    | Empty -> failwith "Empty tree has no minimum"

let rec removeMin =
    function
    | Node(_, Empty, r, _) -> r
    | Node(v, l, r, _) -> balance <| createNode v (removeMin l) r
    | Empty -> Empty

let rec remove value =
    function
    | Empty -> Empty
    | Node(v, l, r, _) as node ->
        if value < v then
            balance <| createNode v (remove value l) r
        elif value > v then
            balance <| createNode v l (remove value r)
        else
            match l, r with
            | Empty, _ -> r
            | _, Empty -> l
            | _ ->
                let minRight = findMin r
                balance <| createNode minRight l (removeMin r)

let rec map func =
    function
    | Empty -> Empty
    | Node(v, l, r, _) -> balance <| createNode (func v) (map func l) (map func r)

let rec fold folder state =
    function
    | Empty -> state
    | Node(v, l, r, _) -> fold folder (fold folder (folder state v) l) r

let foldBack folder state =
    function
    | Empty -> state
    | Node(v, l, r, _) -> fold folder (fold folder (folder state v) r) l

let rec mergeWithInsert tree1 tree2 =
    match tree1 with
    | Empty -> tree2
    | Node(v, l, r, _) ->
        let tree2 = mergeWithInsert l tree2
        let tree2 = mergeWithInsert r tree2
        insert v tree2

let rec find value =
    function
    | Empty -> false
    | Node(v, l, r, _) ->
        if value < v then find value l
        elif value > v then find value r
        else true

let rec toString =
    function
    | Empty -> ""
    | Node(v, l, r, _) ->
        let leftStr = toString l
        let rightStr = toString r
        String.concat "" [ leftStr; sprintf "%A\n" v; rightStr ]

let rec toSortedList =
    function
    | Empty -> []
    | Node(v, l, r, _) ->
        let leftStr = toSortedList l
        let rightStr = toSortedList r
        leftStr @ [ v ] @ rightStr

let listToAVLTree list =
    List.fold (fun acc value -> insert value acc) Empty list

let arrayToAvlTree array = listToAVLTree (Array.toList array)

let equalAVL tree1 tree2 = toSortedList tree1 = toSortedList tree2

let rec filter predicate tree =
    match tree with
    | Empty -> Empty
    | Node(value, left, right, _) ->
        let leftFiltered = filter predicate left
        let rightFiltered = filter predicate right

        if predicate value then
            // Если значение удовлетворяет предикату, вставляем его в новое дерево и балансируем.
            mergeWithInsert leftFiltered rightFiltered |> insert value
        else
            // Если значение не удовлетворяет предикату, объединяем фильтрованные левое и правое поддеревья.
            mergeWithInsert leftFiltered rightFiltered
