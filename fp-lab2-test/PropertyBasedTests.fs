module fp_lab2_test.PropertyTests

open FsCheck
open FsCheck.Xunit
open FsUnit.Xunit
open AVL

let maxValue = 100_000
let count = 10_000

let AvlTreeGenerator =
    gen {
        let values =
            Arb.generate<int>
            |> Gen.sample maxValue count
            |> Seq.toList
            |> List.distinct
            |> listToAVLTree

        return values
    }

type AVLTreeArbitrary() =
    static member Arbitrary: Arbitrary<AVLTree<int>> = Arb.fromGen AvlTreeGenerator

let ArrayGenerator =
    gen {
        let values =
            Arb.generate<int> |> Gen.sample maxValue count |> Seq.toArray |> Array.distinct

        return values
    }

type ArrayArbitrary() =
    static member Arbitrary: Arbitrary<array<int>> =
        Arb.fromGenShrink (ArrayGenerator, Arb.Default.Array().Shrinker)

[<Properties(Arbitrary = [| typeof<AVLTreeArbitrary>; typeof<ArrayArbitrary> |])>]
type AVLTreeProperties() =

    [<Property>]
    let ``Высоты поддеревьев различаются не более чем на один`` (tree: AVLTree<int>) =
        let rec check =
            function
            | Empty -> true
            | Node(_, l, r, _) -> abs (height l - height r) <= 1 && check l && check r

        check tree

    [<Property>]
    let ``Добавленный элемент можно найти в дереве`` (elem: int, tree: AVLTree<int>) =
        let newTree = insert elem tree

        find elem newTree

    [<Property>]
    let ``Удаленный элемент отсутствует в дереве`` (elem: int, tree: AVLTree<int>) =
        let newTree = remove elem tree

        find elem newTree |> should be False

    [<Property>]
    let ``Добавление, затем удаление элемента возвращает исходное дерево`` (elem: int, tree: AVLTree<int>) =
        let tree = remove elem tree // исключаем случаи, когда elem изначально уже есть в tree и вставка ничего не меняет

        let addedTree = insert elem tree
        let removedTree = remove elem addedTree

        equalAVL tree removedTree

    [<Property>]
    let ``Исходные элементы остаются после добавления нового`` (elem: int, tree: AVLTree<int>) =
        let newTree = insert elem tree
        fold (fun state v -> find v newTree && state) true tree |> should be True

    [<Property>]
    let ``min должен возвращать минимальный элемент дерева`` (list: array<int>) =
        let tree = arrayToAvlTree list
        let min = Array.min list

        findMin tree |> should equal min

    [<Property>]
    let ``removeMin должен удалять минимальный элемент дерева`` (list: array<int>) =
        let treeWithoutMin = arrayToAvlTree list |> removeMin
        let min = Array.min list

        find min treeWithoutMin |> should be False

    [<Property>]
    let ``fold должен накапливать результаты обхода по дереву`` (list: array<int>) =
        let tree = arrayToAvlTree list
        let folder = fun acc x -> acc + x
        let sum = fold folder 0 tree

        sum |> should equal (Array.fold folder 0 list)

    [<Property>]
    let ``foldBack должен накапливать результаты обхода по дереву`` (list: array<int>) =
        let tree = arrayToAvlTree list
        let folder = fun acc x -> acc + x
        let sum = foldBack folder 0 tree

        sum |> should equal (Array.fold folder 0 list)

    [<Property>]
    let ``fold и foldBack должны возвращать одинаковый результат обхода по дереву если порядок обхода не имеет значения``
        (list: array<int>)
        =
        let tree = arrayToAvlTree list
        let folder = fun acc x -> acc + x
        let sumFold = fold folder 0 tree
        let sumFoldBack = foldBack folder 0 tree

        sumFold |> should equal sumFoldBack

    // Вспомогательная функция для проверки баланса каждого узла
    let rec checkBalances node =
        match node with
        | Empty -> true
        | Node(_, l, r, _) ->
            let leftHeight = height l
            let rightHeight = height r
            (abs (leftHeight - rightHeight) <= 1) && checkBalances l && checkBalances r

    // Вспомогательная функция для проверки, является ли дерево корректным двоичным деревом поиска
    let rec isProperBST node minVal maxVal =
        match node with
        | Empty -> true
        | Node(v, l, r, _) -> (minVal < v && v < maxVal) && isProperBST l minVal v && isProperBST r v maxVal
    // Каждый узел содержит значение больше, чем значение в любом из узлов его левого поддерева
    // и меньше, чем значение в любом из узлов его правого поддерева.

    let isValidAVLTree tree =
        isProperBST tree System.Int32.MinValue System.Int32.MaxValue
        && checkBalances tree

    [<Property>]
    let ``Дерево AVL должно иметь правильную структуру`` (tree: AVLTree<int>) = isValidAVLTree tree |> should be True

    [<Property>]
    let ``Дерево AVL должно иметь правильную структуру после вставок`` (tree: AVLTree<int>, values: array<int>) =
        let tree = Array.fold (fun acc value -> insert value acc) tree values

        isValidAVLTree tree |> should be True

    [<Property>]
    let ``Дерево AVL должно иметь правильную структуру после удалений`` (tree: AVLTree<int>, values: array<int>) =
        let tree = Array.fold (fun acc value -> remove value acc) tree values

        isValidAVLTree tree |> should be True

    [<Property>]
    let ``Слияние двух деревьев AVL элементы из обоих деревьев должны присутствовать в слитом дереве``
        (
            tree1Values: array<int>,
            tree2Values: array<int>
        ) =
        let tree1 = arrayToAvlTree tree1Values
        let tree2 = arrayToAvlTree tree2Values

        let mergedTree = mergeWithInsert tree1 tree2

        // Проверка, что все элементы из обоих деревьев присутствуют в слитом дереве
        let allValuesPresent =
            Array.forall (fun value -> find value mergedTree) (Array.append tree1Values tree2Values)

        allValuesPresent |> should be True

    [<Property>]
    let ``Слияние двух деревьев AVL должно сохранять свойства AVL`` (tree1: AVLTree<int>, tree2: AVLTree<int>) =
        let mergedTree = mergeWithInsert tree1 tree2

        isValidAVLTree mergedTree |> should be True

    [<Property>]
    let ``Свойство моноида 1: нейтральный элемент`` (tree: AVLTree<int>) =
        let treeAfterInsertEmpty = mergeWithInsert tree Empty

        equalAVL treeAfterInsertEmpty tree

    [<Property>]
    let ``Свойство моноида 2: ассоциативность бинарной операции``
        (
            tree1: AVLTree<int>,
            tree2: AVLTree<int>,
            tree3: AVLTree<int>
        ) =
        let mergedTree1 = mergeWithInsert (mergeWithInsert tree1 tree2) tree3
        let mergedTree2 = mergeWithInsert tree1 (mergeWithInsert tree2 tree3)

        equalAVL mergedTree1 mergedTree2

    [<Property>]
    let ``После фильтрации четных чисел в структуре должны быть только четные числа =)`` (tree: AVLTree<int>) =
        let evenPredicate = fun x -> x % 2 = 0
        let evenNumbersOnly = filter evenPredicate tree // Фильтруем, оставляя только четные числа

        let folder = fun acc x -> acc && (evenPredicate x)
        fold folder true evenNumbersOnly |> should be True

    [<Property>]
    let ``Фильтрации должна сохранять свойства AVL`` (tree: AVLTree<int>) =
        let evenPredicate = fun x -> x % 2 = 0
        let evenNumbersOnly = filter evenPredicate tree

        isValidAVLTree evenNumbersOnly |> should be True
