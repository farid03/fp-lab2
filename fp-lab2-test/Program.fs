module fp_lab2_test.Program

// HACK: fixes value initialization
// https://github.com/dotnet/netcorecli-fsc/issues/79
module Program =
    [<EntryPoint>]
    let main _ = 0
