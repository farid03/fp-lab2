module fp_lab2_test.UnitTests

open Xunit
open FsUnit.Xunit
open AVL

type AVLTreeUnits() =

    [<Fact>]
    let ``Вставка в пустое дерево должна создать дерево с одним узлом`` () =
        let emptyTree = Empty
        let valueToInsert = 42
        let expectedTree = Node(valueToInsert, Empty, Empty, 1)

        let actualTree = insert valueToInsert emptyTree

        actualTree |> should equal expectedTree


    [<Fact>]
    let ``Проверка вставки элементов с балансировкой AVL дерева`` () =
        let valuesToInsert = [ 3; 2; 1; 4; 5 ] // порядок вставки создаст несбалансированное дерево - потребует балансировки
        let actualTree = List.foldBack insert valuesToInsert Empty
        let balanced = (balanceFactor actualTree) |> abs <= 1 // дерево сбалансирвоано

        balanced |> should be True

    [<Fact>]
    [<Trait("Description", "Проверка поиска элемента существующего в дереве")>]
    let ``find должен возвращать значение true, если элемент действительно существует`` () =
        let values = [ 20; 10; 30; 5; 15; 25; 35 ]
        let tree = listToAVLTree values

        let foundNode = find 20 tree
        foundNode |> should be True

    [<Fact>]
    let ``find должен возвращать значение false, если элемент не существует`` () =
        let values = [ 20; 10; 30; 5; 15; 25; 35 ]
        let tree = listToAVLTree values

        let foundNode = find 100 tree
        foundNode |> should be False

    [<Fact>]
    let ``Проверка удаления элемента из AVL дерева`` () =
        let originalTree = listToAVLTree [ 1; 2; 3; 4; 5 ]
        let treeAfterRemoval = remove 3 originalTree

        find 3 treeAfterRemoval |> should be False // элемент удалился

        let balanced = (balanceFactor treeAfterRemoval) |> abs <= 1
        balanced |> should be True // дерево осталось сбалансированным

    [<Fact>]
    [<Trait("Description", "Проверка балансировки после поворотов")>]
    let ``Right rotation on left-heavy tree`` () =
        let leftHeavyTree = listToAVLTree [ 3; 2; 1 ]
        let isBalanced = balanceFactor leftHeavyTree |> abs <= 1

        isBalanced |> should be True

    [<Fact>]
    [<Trait("Description", "Проверка балансировки после поворотов")>]
    let ``Left rotation on right-heavy tree`` () =
        let rightHeavyTree = listToAVLTree [ 1; 2; 3 ]
        let isBalanced = balanceFactor rightHeavyTree |> abs <= 1

        isBalanced |> should be True

    [<Fact>]
    [<Trait("Description", "Проверка балансировки после поворотов")>]
    let ``LeftRight rotation on left-right-heavy tree`` () =
        let leftRightHeavyTree = listToAVLTree [ 3; 1; 2 ]

        let isBalanced = balanceFactor leftRightHeavyTree |> abs <= 1

        isBalanced |> should be True

    [<Fact>]
    [<Trait("Description", "Проверка балансировки после поворотов")>]
    let ``RightLeft rotation on right-left-heavy tree`` () =
        let rightLeftHeavyTree =
            List.fold (fun acc value -> insert value acc) Empty [ 1; 3; 2 ]

        let isBalanced = balanceFactor rightLeftHeavyTree |> abs <= 1

        isBalanced |> should be True

    [<Fact>]
    let ``Тестирование поиска минимального значения в дереве`` () =
        let values = [ 5; 3; 7; 2; 4; 6; 8 ]
        let tree = listToAVLTree values
        let minValue = findMin tree
        minValue |> should equal 2

    [<Fact>]
    let ``Исключение при поиске минимума в пустом дереве`` () =
        let emptyTree = Empty
        Assert.Throws<System.Exception>(fun () -> findMin emptyTree) |> ignore

    [<Fact>]
    [<Trait("Description", "Тестирование функции map, которая применяет функцию ко всем элементам дерева")>]
    let ``map должен применять функцию ко всем элементам дерева`` () =
        let func x = x * 2
        let values = [ 1; 2; 3; 4; 5; 6; 7; 8; 9; 10 ]
        let tree = listToAVLTree values
        let mappedTree = map func tree

        let isDoubled x = (find (func x) mappedTree)
        List.forall isDoubled values |> should be True

    [<Fact>]
    let ``fold должен накапливать результаты обхода по дереву`` () =
        let values = [ 1; 2; 3; 4; 5 ]
        let tree = listToAVLTree values
        let sum = fold (fun acc x -> acc + x) 0 tree

        sum |> should equal (List.sum values)

    [<Fact>]
    let ``Фильтрация четных чисел должна оставлять только четные числа `` () =
        let evenPredicate = fun x -> x % 2 = 0
        let avlTree = listToAVLTree [ 1..100 ] // Создаем AVL дерево с элементами от 1 до 10
        let evenNumbersOnly = filter evenPredicate avlTree // Фильтруем, оставляя только четные числа

        let folder = fun acc x -> acc && (evenPredicate x)
        fold folder true evenNumbersOnly

    // Вспомогательная функция для проверки баланса каждого узла
    let rec checkBalances node =
        match node with
        | Empty -> true
        | Node(_, l, r, _) ->
            let leftHeight = height l
            let rightHeight = height r
            (abs (leftHeight - rightHeight) <= 1) && checkBalances l && checkBalances r

    // Вспомогательная функция для проверки, является ли дерево корректным двоичным деревом поиска
    let rec isProperBST node minVal maxVal =
        match node with
        | Empty -> true
        | Node(v, l, r, _) -> (minVal < v && v < maxVal) && isProperBST l minVal v && isProperBST r v maxVal
    // Каждый узел содержит значение больше, чем значение в любом из узлов его левого поддерева
    // и меньше, чем значение в любом из узлов его правого поддерева.

    let isValidAVLTree tree =
        isProperBST tree System.Int32.MinValue System.Int32.MaxValue
        && checkBalances tree

    [<Fact>]
    let ``Дерево AVL должно иметь правильную структуру после вставок`` () =
        let values = [ 10; 20; 30; 40; 50; 25 ]
        let tree = listToAVLTree values

        isValidAVLTree tree |> should be True

    [<Fact>]
    let ``Слияние двух деревьев AVL должно сохранять свойства дерева AVL`` () =
        let tree1Values = [ 1; 3; 5; 7; 9 ]
        let tree2Values = [ 2; 4; 6; 8; 10 ]
        let tree1 = listToAVLTree tree1Values
        let tree2 = listToAVLTree tree2Values

        let mergedTree = mergeWithInsert tree1 tree2

        // Проверка, что все элементы из обоих деревьев присутствуют в слитом дереве
        let allValuesPresent =
            List.forall (fun value -> find value mergedTree) (tree1Values @ tree2Values)

        allValuesPresent |> should be True
        isValidAVLTree mergedTree |> should be True
